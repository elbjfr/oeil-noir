import React from 'react'
import { connect } from 'react-redux'
import { setProfileAction } from "../store/profile";



class ProfileForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = props.profile;
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit = (event) => {
    event.preventDefault();
    this.props.setProfileAction(this.state);
  }

  handleChange = (event) => {
    this.setState({ [event.target.id]: event.target.value });
  }

  render() {
    console.log(this.state);
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <div>
            <label htmlFor="nom">nom</label>
            <input type="text"
              id="nom"
              value={this.state.nom}
              onChange={this.handleChange}
            />
          </div>
          <div>
            <label htmlFor="archetype">archetype</label>
            <input type="text"
              id="archetype"
              value={this.state.archetype}
              onChange={this.handleChange}
            />
          </div>
          <div>
            <label htmlFor="niveau">niveau</label>
            <input type="number"
              id="niveau"
              value={this.state.niveau}
              onChange={this.handleChange}
            />
          </div>
          <button type="submit">Submit</button>
        </form>
        <p>{this.state.archetype}</p>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return { profile: state.profile };
}

const mapDispatchToProps = { setProfileAction };

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfileForm)
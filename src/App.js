import React from 'react';
import './App.css';
import  ProfileForm  from "./components/profile.form";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <ProfileForm />
      </header>
    </div>
  );
}

export default App;

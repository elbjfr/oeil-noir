const SET_PROFILE = 'SET_PROFILE';

const initProfile = {
    nom:"",
    archetype:"",
    niveau:1
}

export function setProfileAction(profile){
    return {
        type: SET_PROFILE,
        profile
    }
}

export function profileReducer(profile = initProfile, action){
    switch (action.type) {
        case SET_PROFILE:
            return Object.assign({}, profile, action.profile);
        default:
            return profile;
    }
}